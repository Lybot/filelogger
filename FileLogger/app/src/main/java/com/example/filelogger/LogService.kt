package com.example.filelogger

import android.app.Notification
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.*
import android.support.v4.app.NotificationCompat
import java.io.File
import java.io.FileWriter
import java.text.SimpleDateFormat
import java.util.*

class LogService(private var notificationManager: NotificationManager? = null) : Service() {
    private lateinit var _monitors:MutableSet<FileObserver>
    private val _id = 322

    override fun onBind(intent: Intent): IBinder {
        return Binder()
    }

    override fun onCreate() {
        super.onCreate()
        notificationManager =getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        sendNotification("Сервис","Аудит","Запущен")
        try{
        if (!_monitors.isEmpty()) {
            _monitors.iterator().forEach {
                it.stopWatching()
            }
            _monitors = mutableSetOf()
        }
        }
        catch (e:Exception) {
            _monitors = mutableSetOf()
        }
        val preferences = applicationContext.getSharedPreferences("app",Context.MODE_PRIVATE)
        val set = preferences.getStringSet("listDirectories", mutableSetOf(Environment.getExternalStorageDirectory().absolutePath))
        val edit = preferences.edit()
        val dot = preferences.getString("dot",",")
        val logName = preferences.getString("logName", Build.MODEL)
//        val formatPath = preferences.getBoolean("formatPath",false)
        set!!.iterator().forEach {
            try {
                val stack = Stack<String>()
                stack.push(it)
                var file: File
                var path: String
                var files: Array<File>?
                while (!stack.empty()) {
                    path = stack.pop()
                    _monitors.add(object : FileObserver(path, FileObserver.ALL_EVENTS) {
                        private var logString:String = ""
                        private val truePath:String=path
                        override fun onEvent(i: Int, s: String?) {
                            try {
                                if (s!=null && i==FileObserver.OPEN) {
                                    val date = Date()
                                    val formatter = SimpleDateFormat("dd.MM.yyyy, HH:mm", Locale.getDefault())
//                                    if (formatPath) {
//                                        s.replace(",", "")
//                                        s.replace(";","")
//                                    }
                                    logString="$truePath/$s"+dot+formatter.format(date)
                                    if (s.endsWith("pdf")||s.endsWith("doc")||s.endsWith("docx")||s.endsWith("xls")||s.endsWith("xlsx")){
                                        val writer =
                                            FileWriter(
                                                File(getExternalFilesDir("files")!!.path+"/" + logName+".csv"),
                                                true
                                            )
                                        writer.append(logString)
                                        writer.append('\n')
                                        writer.close()
                                        val setLog = preferences.getStringSet("setLog", mutableSetOf())
                                        setLog!!.add(logString)
                                        edit.putStringSet("setLog",setLog)
                                        edit.apply()
                                        logString=""
                                    }
                                }
                                else{
                                    if (logString!="" && s==null) {
                                        val writer =
                                            FileWriter(
                                                File(getExternalFilesDir("files")!!.path+"/" + logName +".csv"),
                                                true
                                            )
                                        writer.append(logString)
                                        writer.append('\n')
                                        writer.close()
                                        val setLog = preferences.getStringSet("setLog", mutableSetOf())
                                        setLog!!.add(logString)
                                        edit.putStringSet("setLog",setLog)
                                        edit.apply()
                                        logString=""
                                    }
                                }
                            } catch (e: Exception) {
                            }
                        }
                    })

                    file = File(path)
                    files = file.listFiles()
                    if (files == null)
                        continue

                    for (i in files.indices) {
                        if (files[i].isDirectory
                            && files[i].exists()
                            && files[i].name != "."
                            && files[i].name != ".."
                        ) {
                            stack.push(files[i].path)
                        }
                    }
                }
            }
                catch (e:Exception)
                {
                    val newService = Intent(applicationContext,LogService::class.java)
                    startService(newService)
                }
        }
        if (!_monitors.isEmpty())
        _monitors.iterator().forEach {
            it.startWatching()
        }
        return START_STICKY
    }
    private fun sendNotification(Ticker: String, Title: String, Text: String) {

        //These three lines makes Notification to open main activity after clicking on it
//        val notificationIntent = Intent(this, MainActivity::class.java)
//        notificationIntent.action = Intent.ACTION_MAIN
//        notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER)
//        val contentIntent =
//            PendingIntent.getActivity(applicationContext, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        val builder = NotificationCompat.Builder(this,_id.toString())
//        builder.setContentIntent(contentIntent)
            .setOngoing(true)   //Can't be swiped out
            .setSmallIcon(R.mipmap.ic_launcher)
            //.setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.large))   // большая картинка
            .setTicker(Ticker)
            .setContentTitle(Title) //Заголовок
            .setContentText(Text) // Текст уведомления
            .setWhen(System.currentTimeMillis())

        val notification: Notification
            notification = builder.build()
        startForeground(_id, notification)
    }
}
