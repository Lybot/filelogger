package com.example.filelogger

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.android.synthetic.main.new_password_alert.view.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val preferences = getSharedPreferences("settings", Context.MODE_PRIVATE)
        val edit = preferences.edit()
        val password = preferences.getString("passwordApp","firstTimeLaunchQwerty")
        findViewById<Button>(R.id.enterButton).setOnClickListener {
            if (password == "firstTimeLaunchQwerty") {
                val alert = AlertDialog.Builder(this)
                val view = layoutInflater.inflate(R.layout.new_password_alert,null)
                view.saveChangesButton.setOnClickListener {
                    if (view.passwordEditText.text.toString()!="")
                    {
                        edit.putString("passwordApp",view.passwordEditText.text.toString())
                        edit.apply()
                        val intent = Intent(this, MainActivity::class.java)
                        startActivity(intent)
                    }
                    else
                        Toast.makeText(this,R.string.wrong_password,Toast.LENGTH_LONG).show()
                }
                alert.setView(view)
                alert.show()
            }
            else{
                if (password==passwordEditText.text.toString())
                {
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                }
                else
                    Toast.makeText(this,R.string.wrong_password,Toast.LENGTH_LONG).show()
            }
        }
    }
}
