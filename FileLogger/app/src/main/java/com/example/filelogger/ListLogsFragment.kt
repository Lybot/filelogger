package com.example.filelogger


import android.app.AlertDialog
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import kotlinx.android.synthetic.main.fragment_list_logs.view.*
import java.io.File
import java.io.FileReader


class ListLogsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view= inflater.inflate(R.layout.fragment_list_logs, container, false)
        val preferences = context!!.getSharedPreferences("app",Context.MODE_PRIVATE)
        var set = preferences.getStringSet("setLog", mutableSetOf("empty","logs"))!!.toList()
        val edit = preferences.edit()
        val adapter = ArrayAdapter<String>(context!!,android.R.layout.simple_list_item_1,set)
        view.findViewById<ListView>(R.id.logs_list).adapter=adapter
        view.clearLogsButton.setOnClickListener {
            val alert = AlertDialog.Builder(activity)
            alert.setTitle(R.string.delete)
            alert.setMessage(R.string.delete_logs)
            alert.setPositiveButton(R.string.delete_screen
            ) { dialog, _ ->
                set= listOf()
                edit.putStringSet("setLog",set.toMutableSet())
                edit.apply()
                val newAdapter = ArrayAdapter<String>(context!!,android.R.layout.simple_list_item_1,set)
                view.findViewById<ListView>(R.id.logs_list).adapter=newAdapter
                dialog.cancel()
            }
            alert.setNegativeButton(R.string.delete_file) { dialog, _ ->
                val file = File(activity!!.getExternalFilesDir("files")!!.path+"/" + BluetoothAdapter.getDefaultAdapter().name+".csv")
                file.delete()
                file.createNewFile()
                set= listOf()
                edit.putStringSet("setLog",set.toMutableSet())
                edit.apply()
                val newAdapter = ArrayAdapter<String>(context!!,android.R.layout.simple_list_item_1,set)
                view.findViewById<ListView>(R.id.logs_list).adapter=newAdapter
                dialog.cancel()
            }
            alert.show()
        }
        view.downloadLogsButton.setOnClickListener {
            val fr = FileReader(File(activity!!.getExternalFilesDir("files")!!.path+"/" + BluetoothAdapter.getDefaultAdapter().name+".csv"))
            set = fr.readLines()
            fr.close()
            edit.putStringSet("setLog",set.toMutableSet())
            edit.apply()
            val newAdapter = ArrayAdapter<String>(context!!,android.R.layout.simple_list_item_1,set)
            view.findViewById<ListView>(R.id.logs_list).adapter=newAdapter
        }
        return view
    }


}
