package com.example.filelogger

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class AutoLaunchReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val action =intent.action
        if (action==Intent.ACTION_BOOT_COMPLETED||action==Intent.ACTION_LOCKED_BOOT_COMPLETED) {
            val intentService = Intent(context, LogService::class.java)
            context.startService(intentService)
            val intentSendService = Intent(context, SendLogsService::class.java)
            context.startService(intentSendService)

        }
    }
}
