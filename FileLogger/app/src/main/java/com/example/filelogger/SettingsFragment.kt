package com.example.filelogger

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.NotificationCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import it.sauronsoftware.ftp4j.FTPClient
import kotlinx.android.synthetic.main.fragment_settings.view.*
import okhttp3.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class SettingsFragment : Fragment() {
    private val _id = 101
    private var client = OkHttpClient.Builder()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_settings, container, false)
        val preferences = context!!.getSharedPreferences("settings", Context.MODE_PRIVATE)
        val editPreferences = preferences.edit()
        val password = preferences.getString("password","")
        val login = preferences.getString("login","")
        val address = preferences.getString("address","")
        val port = preferences.getInt("port",21)
        val frequency = preferences.getInt("frequency",24)
        val switch1 = preferences.getBoolean("http/ftp",false)
        val switch2 = preferences.getBoolean("https",false)
        val pref = context!!.getSharedPreferences("app",Context.MODE_PRIVATE)
        val edit = pref.edit()
        val logName = pref.getString("logName",Build.MODEL)
//        val formatPath = pref.getBoolean("formatPath",false)
        view.addressEditText.setText(address)
        view.logNameEditText.setText(logName)
//        view.formatPathSwitch.setOnCheckedChangeListener { _, isChecked ->
//            if (isChecked)
//            {
//                view.formatPathSwitch.setText(R.string.format_path_off)
//                edit.putBoolean("formatPath",true)
//                edit.apply()
//            }
//            else{
//                view.formatPathSwitch.setText(R.string.format_path_on)
//                edit.putBoolean("formatPath",false)
//                edit.apply()
//            }
//        }
//        view.formatPathSwitch.isChecked=formatPath
        view.changePasswordButton.setOnClickListener {
            if (view.changePasswordEditText.text.toString()!=view.confirmPasswordEditText.text.toString())
            {
                Toast.makeText(context,R.string.toast_change_password_error,Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            editPreferences.putString("passwordApp", view.changePasswordEditText.text.toString())
            Toast.makeText(context,R.string.alert_dialog_save_changes_message,Toast.LENGTH_LONG).show()
            editPreferences.apply()
        }
        view.ftpHttpSwitch.setOnCheckedChangeListener { it, isChecked ->
            if (isChecked) {
                it.setText(R.string.ftp)
                editPreferences.putBoolean("http/ftp", true)
                view.httpHttpsSwitch.visibility=ViewGroup.GONE
                view.authLinearLayout.visibility=ViewGroup.VISIBLE
                view.portEditText.visibility=ViewGroup.VISIBLE
                if (login!=""){
                view.passwordEditText.setText(password)
                view.loginEditText.setText(login)}
                view.portEditText.setText(port.toString())
            }
            else{
                it.setText(R.string.http)
                editPreferences.putBoolean("http/ftp", false)
                view.authLinearLayout.visibility=ViewGroup.GONE
                view.httpHttpsSwitch.visibility=ViewGroup.VISIBLE
                view.portEditText.visibility=ViewGroup.GONE
            }
        }
        view.httpHttpsSwitch.setOnCheckedChangeListener { it, isChecked ->
            if (isChecked) {
                it.setText(R.string.https)
                editPreferences.putBoolean("https", true)
            }
            else{
                it.setText(R.string.http)
                editPreferences.putBoolean("https", false)
            }
        }

        view.confirmSettingsButton.setOnClickListener {
            editPreferences.putString("login",view.loginEditText.text.toString())
            editPreferences.putString("password",view.passwordEditText.text.toString())
            editPreferences.putInt("frequency",view.frequencyEditText.text.toString().toInt())
            editPreferences.putString("address",view.addressEditText.text.toString())
            editPreferences.apply()
            edit.putString("logName",view.logNameEditText.text.toString())
            edit.apply()
            val intent = Intent(context,SendLogsService::class.java)
            context!!.startService(intent)
        }
        view.ftpHttpSwitch.isChecked=switch1
        view.httpHttpsSwitch.isChecked=switch2
        view.dotSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked)
            {
                buttonView.setText(R.string.dot_with_comma)
                edit.putString("dot",";")
                edit.apply()
            }
            else{
                buttonView.setText(R.string.comma)
                edit.putString("dot",",")
                edit.apply()
            }
        }
        view.dotSwitch.isChecked= pref?.getString("dot",",").equals(";")
        view.frequencyEditText.setText(frequency.toString())
        view.testButton.setOnClickListener {
            val testAddress = view.addressEditText.text.toString()
            val date = Date()
            val formatter = SimpleDateFormat("ddMMyy_HHmm", Locale.getDefault())
            val fileTime= File(activity!!.getExternalFilesDir("files")!!.path+"/" + logName+"_"+formatter.format(date)+ ".csv")
            val fileLog = File(activity!!.getExternalFilesDir("files")!!.path+"/" + logName+".csv")
            fileLog.copyTo(fileTime)
            if (view.ftpHttpSwitch.isChecked) {
                val testLogin = view.loginEditText.text.toString()
                val testPassword = view.passwordEditText.text.toString()
                val testPort = view.portEditText.text.toString().toInt()
                AsyncTask.execute {
                    try{
                    val ftpClient = FTPClient()
                    ftpClient.connect(testAddress,testPort)
                        try {
                            ftpClient.login(testLogin, testPassword)
                        }
                        catch (e:Exception){
                            sendIncorrectLogin()
                            return@execute
                        }
                        ftpClient.type = FTPClient.TYPE_BINARY
                        ftpClient.upload(fileTime)
                        ftpClient.disconnect(true)
                        sendNotification(true)
                        fileTime.delete()
                }
                catch (e:Throwable){
                    sendNotification(false)
                    fileTime.delete()
                }
                }
            }
            else{
                if (view.httpHttpsSwitch.isChecked)
                {
                    try {
                        uploadFile("https://$testAddress",fileTime)
                    }
                    catch (e:Exception){
                    }


                }
                else {
                    try {
                        uploadFile("http://$testAddress", fileTime)
                    }
                    catch (e: Exception) {
                    }
                }
            }
        }
        return view
    }
    private fun sendNotification(success:Boolean) {
        val title:String
        val text:String
        if(success){
            title = resources.getString(R.string.good_request_title)
            text = resources.getString(R.string.good_request_text)
        }
        else{
            title = resources.getString(R.string.bad_request_title)
            text = resources.getString(R.string.bad_request_text)
        }
        val builder = NotificationCompat.Builder(context!!,_id.toString())
//            .setOngoing(true)   //Can't be swiped out
            .setSmallIcon(R.mipmap.ic_launcher)
            //.setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.large))   // большая картинка
            .setContentTitle(title) //Заголовок
            .setContentText(text) // Текст уведомления
//            .setWhen(System.currentTimeMillis())
            .setAutoCancel(true)
        val notification: Notification
        notification = builder.build()
        val notificationManager = activity!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT>25) {
            val channel= NotificationChannel(_id.toString(),_id.toString(), NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(channel)
            notificationManager.notify(_id,notification)
        }
        else {
            notificationManager.notify(_id, notification)
        }
    }
    private fun uploadFile(serverURL: String, file: File): Boolean {
        try {

            val requestBody = MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart(
                    "file", file.name,
                    RequestBody.create(MediaType.parse("text/csv"), file)
                )
                .build()

            val request = Request.Builder()
                .url(serverURL)
                .post(requestBody)
                .build()
            client.build().newCall(request).enqueue(object: Callback {

                override fun onFailure(call: Call, e: IOException) {
                    sendNotification(false)
                    file.delete()
                }

                @Throws(Exception::class)
                override fun onResponse(call: Call, response: Response) {
                    if (!response.isSuccessful) {
                        sendNotification(false)
                        file.delete()
                    }
                    sendNotification(true)
                    file.delete()
                }
            })

            return true
        } catch (ex: Exception) {
            sendNotification(false)
            file.delete()
        }

        return false
    }
    private fun sendIncorrectLogin() {
        val title = "Ошибка"
        val text = "Неправильный логин/пароль"
        val builder = NotificationCompat.Builder(context!!,_id.toString())
//            .setOngoing(true)   //Can't be swiped out
            .setSmallIcon(R.mipmap.ic_launcher)
            //.setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.large))   // большая картинка
            .setContentTitle(title) //Заголовок
            .setContentText(text) // Текст уведомления
//            .setWhen(System.currentTimeMillis())
            .setAutoCancel(true)
        val notification: Notification
        notification = builder.build()
        val notificationManager = activity!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT>25) {
            val channel= NotificationChannel(_id.toString(),_id.toString(), NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(channel)
            notificationManager.notify(_id,notification)
        }
        else {
            notificationManager.notify(_id, notification)
        }
    }
}
