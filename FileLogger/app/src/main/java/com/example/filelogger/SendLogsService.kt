package com.example.filelogger

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.text.format.DateUtils
import android.util.Log
import it.sauronsoftware.ftp4j.FTPClient
import okhttp3.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class SendLogsService : Service() {
    private val _id = 321
    private var client = OkHttpClient.Builder()
    override fun onBind(intent: Intent): IBinder {
        return Binder()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        client = OkHttpClient.Builder()
        val preferences = getSharedPreferences("settings", Context.MODE_PRIVATE)
        val prefApp = getSharedPreferences("app",Context.MODE_PRIVATE)
        val typeFtp = preferences.getBoolean("http/ftp",false)
        val address =preferences.getString("address","localhost")
        var frequency = preferences.getInt("frequency",60)
        if (frequency!=60)
            frequency= (24*60/frequency)
        nextUpdate(frequency)
        val logName = prefApp.getString("logName",Build.MODEL)
        val fileLog = File(getExternalFilesDir("files")!!.path+"/" + logName+".csv")
        if (fileLog.length()>0) {
            val date = Date()
            val formatter = SimpleDateFormat("ddMMyy_HHmm", Locale.getDefault())
            val fileTime = File(
                getExternalFilesDir("files")!!.path + "/" + logName + "_" + formatter.format(
                    date
                ) + ".csv"
            )
            fileLog.copyTo(fileTime)
            if (typeFtp) {
                val login = preferences.getString("login", null)
                val password = preferences.getString("password", null)
                val port = preferences.getInt("port", 21)
                AsyncTask.execute {
                    try {
                        val ftpClient = FTPClient()
                        ftpClient.connect(address, port)
                        ftpClient.login(login, password)
                        ftpClient.type = FTPClient.TYPE_BINARY
                        ftpClient.upload(fileTime)
                        ftpClient.disconnect(true)
                        sendNotification(true)
                        fileTime.delete()
                        fileLog.delete()
                        fileLog.createNewFile()
                    } catch (e: Throwable) {
                        sendNotification(false)
                        fileTime.delete()
                    }
                }

                return START_NOT_STICKY
            }
            val https = preferences.getBoolean("https", false)
            if (https) {
                try {
                    uploadFile("https://$address", fileTime, fileLog)
                } catch (e: Exception) {
                    Log.d("ConnectService", e.toString())
                }
                return START_NOT_STICKY

            }
            try {
                uploadFile("http://$address", fileTime, fileLog)
            } catch (e: Exception) {
                Log.d("ConnectService", e.toString())
            }
        }
        return START_NOT_STICKY
    }
    private fun nextUpdate(minutes:Int){
        val intent = Intent(this, this.javaClass)
        val pendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        val currentTimeMillis = System.currentTimeMillis()
        val nextUpdateTimeMillis = currentTimeMillis + minutes * DateUtils.MINUTE_IN_MILLIS
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.set(AlarmManager.RTC, nextUpdateTimeMillis, pendingIntent)
    }

    private fun uploadFile(serverURL: String, fileTime: File,fileLog:File): Boolean {
        try {

            val requestBody = MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart(
                    "file", fileTime.name,
                    RequestBody.create(MediaType.parse("text/csv"), fileTime)
                )
                .build()

            val request = Request.Builder()
                .url(serverURL)
                .post(requestBody)
                .build()
            client.build().newCall(request).enqueue(object:Callback {

                override fun onFailure(call: Call, e: IOException) {
                    sendNotification(false)
                    fileTime.delete()
                }

                @Throws(Exception::class)
                override fun onResponse(call: Call, response: Response) {
                    if (!response.isSuccessful) {
                        sendNotification(false)
                        fileTime.delete()
                    }
                    sendNotification(true)
                    fileTime.delete()
                    fileLog.delete()
                    fileLog.createNewFile()

                }
            })

            return true
        } catch (ex: Exception) {
            sendNotification(false)
            fileTime.delete()
        }

        return false
    }
    private fun sendNotification(success: Boolean) {
        val title:String
        val text:String
        if(success){
            title = resources.getString(R.string.good_request_title)
            text = resources.getString(R.string.good_request_text)
        }
        else{
            title = resources.getString(R.string.bad_request_title)
            text = resources.getString(R.string.bad_request_text)
        }
        val builder = NotificationCompat.Builder(this,_id.toString())
//            .setOngoing(true)   //Can't be swiped out
            .setSmallIcon(R.mipmap.ic_launcher)
            //.setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.large))   // большая картинка
            .setContentTitle(title) //Заголовок
            .setContentText(text) // Текст уведомления
//            .setWhen(System.currentTimeMillis())
            .setAutoCancel(true)
        val notification: Notification
        notification = builder.build()
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT>25) {
            val channel=NotificationChannel(_id.toString(),_id.toString(),NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(channel)
            notificationManager.notify(_id,notification)
        }
        else {
            notificationManager.notify(_id, notification)
        }
    }
}
