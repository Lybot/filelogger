package com.example.filelogger

import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.bluetoth_name_alert.view.*
import java.io.File


class MainActivity : AppCompatActivity() {
    private val fc = this.supportFragmentManager
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                val transac = fc.beginTransaction()
                val fragment = LogFragment()
                transac.replace(R.id.frame_layout,fragment)
                transac.commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                val transac = fc.beginTransaction()
                val fragment = ListLogsFragment()
                transac.replace(R.id.frame_layout,fragment)
                transac.commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {
                val transac = fc.beginTransaction()
                val fragment = SettingsFragment()
                transac.replace(R.id.frame_layout,fragment)
                transac.commit()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val pref = getSharedPreferences("app", Context.MODE_PRIVATE)
        val editPref = pref.edit()
        if (Build.VERSION.SDK_INT>22)
            requestPermissions(arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE,android.Manifest.permission.RECEIVE_BOOT_COMPLETED,android.Manifest.permission.BLUETOOTH_ADMIN),228)
        try {
            if (BluetoothAdapter.getDefaultAdapter().name == Build.MODEL) {
                val alert = AlertDialog.Builder(this)
                val dialog = alert.create()
                val view = layoutInflater.inflate(R.layout.bluetoth_name_alert, null)
                view.saveChangesButton.setOnClickListener {
                    if (view.bluetoothNameEditText.text.toString() != "") {
                        editPref.putString("logName",view.bluetoothNameEditText.text.toString())
                        editPref.apply()
                    } else
                        Toast.makeText(this, R.string.incorrect_name, Toast.LENGTH_LONG).show()
                }
                view.cancelButton.setOnClickListener {
                    dialog.dismiss()
                }
                dialog.setView(view)
                dialog.show()
            }
            else{
                editPref.putString("logName",BluetoothAdapter.getDefaultAdapter().name)
            }
        }
        catch (e:Exception){
            Toast.makeText(this,"Отсутствует блютуз",Toast.LENGTH_LONG).show()
        }
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        try {
            val file =
                File(getExternalFilesDir("files")!!.path + "/" + pref.getString("logName",Build.MODEL) + ".csv")
            val isNewFileCreated: Boolean = file.createNewFile()
            if (isNewFileCreated) {
                println("log file is created successfully.")
            } else {
                println("log file already exists.")
            }
        }
        catch (e:Exception){
        }
        }

    override fun onResume() {
        super.onResume()
        val transac = fc.beginTransaction()
        val fragment = ListLogsFragment()
        transac.replace(R.id.frame_layout,fragment)
        transac.commit()
        navigation.selectedItemId = R.id.navigation_dashboard
    }

    }


