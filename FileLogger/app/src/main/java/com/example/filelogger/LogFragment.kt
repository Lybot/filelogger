package com.example.filelogger

import android.app.ActivityManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat.getColor
import android.support.v4.content.ContextCompat.getSystemService
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import kotlinx.android.synthetic.main.fragment_log.view.*


@Suppress("DEPRECATION")
class LogFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_log, container, false)
        val preferences= context!!.getSharedPreferences("app", Context.MODE_PRIVATE)
        val edit = preferences.edit()
        val listDirectories =  preferences.getStringSet("listDirectories",mutableSetOf())!!.toMutableList()
        view.findViewById<Button>(R.id.selectFilesButton).setOnClickListener {
            val dialog = DirectorySelectorDialog.Builder(context)
                .directory(Environment.getExternalStorageDirectory())
                .sortBy(DirectorySelectorDialog.SORT_NAME)
                .orderBy(DirectorySelectorDialog.ORDER_ASCENDING)
                .build()
            dialog.addDirectorySelectorListener { directory ->
                listDirectories.add(directory.path)
                edit.putStringSet("listDirectories",listDirectories.toMutableSet())
                edit.apply()
                view.findViewById<ListView>(R.id.directoryListView).adapter =
                    ArrayAdapter<String>(this@LogFragment.context!!, android.R.layout.simple_list_item_1,listDirectories.toMutableSet().toMutableList())
            }
            dialog.show()
        }
        val adapter = ArrayAdapter<String>(context!!, android.R.layout.simple_list_item_1,listDirectories)
        view.findViewById<ListView>(R.id.directoryListView).adapter = adapter
        val logService = ComponentName(activity!!,LogService::class.java)
        if(isMyServiceRunning(logService)) {
            view.serviceWorkingTextView.setText(R.string.service_working)
            view.serviceWorkingTextView.setTextColor(getColor(activity!!.applicationContext,R.color.colorPrimaryDark))
            view.startServiceButton.setText(R.string.stop_service)
            view.startServiceButton.setBackgroundColor(resources.getColor(android.R.color.holo_red_dark))
        }
        view.findViewById<ListView>(R.id.directoryListView).setOnItemClickListener { _, _, position, _ ->
            val alert = AlertDialog.Builder(activity!!)
            alert.setMessage(R.string.alert_dialog_delete_message)
            alert.setTitle(R.string.alert_dialog_delete_title)
            alert.setPositiveButton(getString(R.string.delete)) { dialog, _ ->
                listDirectories.removeAt(position)
                adapter.notifyDataSetChanged()
                edit.putStringSet("listDirectories",listDirectories.toMutableSet())
                edit.apply()
                dialog.cancel()
            }
            alert.setNegativeButton(getString(R.string.cancel)) { dialog, _ ->
                dialog.cancel()
            }
            val alert2 =alert.create()
            alert2.show()
        }
        view.startServiceButton.setOnClickListener {
            if (!isMyServiceRunning(logService)) {

                val service = Intent(context, LogService::class.java)
                if (Build.VERSION.SDK_INT<26)
                {
                    context!!.startService(service)
                }
                else
                {
                    context!!.startForegroundService(service)
                }
                view.serviceWorkingTextView.setText(R.string.service_working)
                view.serviceWorkingTextView.setTextColor(resources.getColor(R.color.colorPrimaryDark))
                view.startServiceButton.setText(R.string.stop_service)
                view.startServiceButton.setBackgroundColor(resources.getColor(android.R.color.holo_red_dark))
            }
            else{
                val service = Intent(context, LogService::class.java)
                context!!.stopService(service)
                view.startServiceButton.setText(R.string.start_service)
                view.startServiceButton.setBackgroundColor(resources.getColor(R.color.colorPrimaryDark))
                view.serviceWorkingTextView.setText(R.string.service_not_working)
                view.serviceWorkingTextView.setTextColor(resources.getColor(android.R.color.holo_red_dark))
            }
        }
        return view
    }
    private fun isMyServiceRunning(serviceClass: ComponentName): Boolean {
        val manager = getSystemService(context!!,ActivityManager::class.java)
        try {
            for (service in manager!!.getRunningServices(Int.MAX_VALUE)) {
                if (serviceClass.className == service.service.className) {
                    return true
                }
            }
            return false
        }
        catch (e:Exception)
        {
            return false
        }
    }
}


